/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Examen;

/**
 *
 * @author vitic
 */
public abstract class Empleado {
    // Atributos
    protected int numEmpleado;
    protected String nombre;
    protected String domicilio;
    protected float pagoDia;
    protected int diasTrabajados;
    protected int porcentajeISR;
    
    // Constructores
    public Empleado() {
        this.numEmpleado = 0;
        this.nombre = "";
        this.domicilio = "";
        this.pagoDia = 0.0f;
        this.diasTrabajados = 0;
        this.porcentajeISR = 0;
    }
    
    public Empleado(int numEmpleado, String nombre, String domicilio, float pagoDia, int diasTrabajados, int porcentajeISR) {
        this.numEmpleado = numEmpleado;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.pagoDia = pagoDia;
        this.diasTrabajados = diasTrabajados;
        this.porcentajeISR = porcentajeISR;
    }
    
    // Encapsulado
    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public float getPagoDia() {
        return pagoDia;
    }

    public void setPagoDia(float pagoDia) {
        this.pagoDia = pagoDia;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }

    public int getPorcentajeISR() {
        return porcentajeISR;
    }

    public void setPorcentajeISR(int porcentajeISR) {
        this.porcentajeISR = porcentajeISR;
    }
    
    // Métodos
    public abstract float calcularPago();
    public abstract float calcularImpuesto();
}
