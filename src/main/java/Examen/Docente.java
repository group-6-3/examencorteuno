/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Examen;

/**
 *
 * @author vitic
 */
public class Docente extends Empleado{
    // Atributos
    private String nivel;
    
    // Constructores
    public Docente() {
        super();
        this.nivel = "";
    }
    
    public Docente(int numEmpleado, String nombre, String domicilio, float pagoDia, int diasTrabajados, int porcentajeISR, String nivel) {
        super(numEmpleado, nombre, domicilio, pagoDia, diasTrabajados, porcentajeISR);
        this.nivel = nivel;
    }
    
    // Encapsulado
    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }
    
    // Métodos
    @Override
    public float calcularPago() {
        String nivelEscolar = this.getNivel();
        float pagoAdicional = 1.0f;
        
        switch(nivelEscolar){
            case "PreEscolar": pagoAdicional = 1.35f; break;
            case "Primaria": pagoAdicional = 1.40f; break;
            case "Secundaria": pagoAdicional = 1.50f; break;
        }
        
        return (this.getPagoDia() * this.getDiasTrabajados()) * pagoAdicional;
    }

    @Override
    public float calcularImpuesto() {
        return this.calcularPago() * ((float) this.getPorcentajeISR() / 100);
    }
}
