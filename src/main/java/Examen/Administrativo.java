/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Examen;

/**
 *
 * @author vitic
 */
public class Administrativo extends Empleado{
    // Atributos
    private String tipoContrato;
    
    // Constructores
    public Administrativo() {
        super();
        this.tipoContrato = "";
    }

    public Administrativo(int numEmpleado, String nombre, String domicilio, float pagoDia, int diasTrabajados, int porcentajeISR, String tipoContrato) {
        super(numEmpleado, nombre, domicilio, pagoDia, diasTrabajados, porcentajeISR);
        this.tipoContrato = tipoContrato;
    }
    
    // Encapsulado
    public String getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(String tipoContrato) {
        this.tipoContrato = tipoContrato;
    }
     
    // Métodos
    public float calcularRetencion(){
        float pago = this.calcularPago();
        float retencion;
        
        if(pago > 10000){
            retencion = 0.1f;
        }
        else if(pago > 5000){
            retencion = 0.08f;
        }
        else{
            retencion = 0.05f;
        }
        
        return pago * retencion;
    }
    
    @Override
    public float calcularPago() {
        return this.getPagoDia() * this.getDiasTrabajados();
    }

    @Override
    public float calcularImpuesto() {
        return (this.calcularPago() - this.calcularRetencion()) * (this.getPorcentajeISR() / 100);
    }
    
}
